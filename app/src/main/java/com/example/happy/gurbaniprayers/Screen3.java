package com.example.happy.gurbaniprayers;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class Screen3 extends ActionBarActivity {
String pathname;
    TextView tv5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        setContentView(R.layout.activity_screen3);
        Intent i=getIntent();
        Bundle b=i.getExtras();
         pathname=b.getString("pn");
        TextView tv4= (TextView) findViewById(R.id.textView4);
        tv5= (TextView) findViewById(R.id.textView5);
        tv4.setText(pathname);
        if(pathname.equals("Japji Sahib"))
        {
            tv5.setText(R.string.japjiSahib);
        }
    else if(pathname.equals("Jaap Sahib"))
        {tv5.setText(R.string.jaapSahib);
         }
        else if(pathname.equals("Tav Prasad Savaiye"))
        {tv5.setText(R.string.Savaiye);}
        else if(pathname.equals("Chaupai Sahib"))
        {tv5.setText(R.string.chaupaiSahib);}
        else if(pathname.equals("Anand Sahib"))
        {tv5.setText(R.string.anandSahib);}
        else if(pathname.equals("Rehraas Sahib"))
        {tv5.setText(R.string.rehraasSahib);}
        else if(pathname.equals("Sohila Sahib"))
        {tv5.setText(R.string.KirtanSohila);}

        else if(pathname.equals("Ardaas"))
        {tv5.setText(R.string.Ardaas);}


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_screen3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
