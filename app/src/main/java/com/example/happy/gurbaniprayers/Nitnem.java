package com.example.happy.gurbaniprayers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.nfc.Tag;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Iterator;


public class Nitnem extends ActionBarActivity {
ListView lv1;
    ToggleButton t1;
    Button b1;
    String allow;
    ArrayAdapter ap;
    MediaPlayer main;
    String pathname;
    String click;
    ImageButton ib1,ib2;
    String name[]={"JAPJI SAHIB","JAAP SAHIB","TAV PRASAD SVAIYE","CHAUPAI SAHIB","ANAND SAHIB","REHRAAS SAHIB","SOHILA SAHIB","ARDAAS"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_nitnem);
        ib1= (ImageButton) findViewById(R.id.imageButton);
        t1= (ToggleButton) findViewById(R.id.toggleButton);
        ib2= (ImageButton) findViewById(R.id.imageButton2);
        final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.1F);
        lv1= (ListView) findViewById(R.id.listView);
        lv1.setVisibility(View.INVISIBLE);
        ap=new ArrayAdapter(this,R.layout.listviewlayout,R.id.tvlv1,name);
        lv1.setAdapter(ap);
        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean on =t1.isChecked();
                if(on)
                {v.startAnimation(buttonClick);
                    lv1.setVisibility(View.VISIBLE);

                }
                else
                {v.startAnimation(buttonClick);
                    lv1.setVisibility(View.INVISIBLE);}
            }
        });






        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.japjisahib);
                    pathname="Japji Sahib";}
                if (position == 1) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.jaapsahib);
                    pathname="Jaap Sahib";}
                if (position == 2) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.savaiye);
                    pathname="Tav Prasad Savaiye";}
                if (position == 3) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }
                    main = MediaPlayer.create(Nitnem.this, R.raw.chaupaisahib);
                    pathname="Chaupai Sahib";}
                if (position == 4) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.anandsahib);
                    pathname="Anand Sahib";}
                if (position == 5) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.rehraassahib);
                    pathname="Rehraas Sahib";}
                if (position == 6) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }

                    main = MediaPlayer.create(Nitnem.this, R.raw.kirtansohila);
                    pathname="Sohila Sahib";}
                if (position == 7) {
                    try {
                        if (main.isPlaying() == true) {
                            main.release();
                        }
                    } catch (Exception e) {
                    }
                    main = MediaPlayer.create(Nitnem.this, R.raw.ardaas);
                    pathname="Ardaas";}
                AlertDialog.Builder build=new AlertDialog.Builder(Nitnem.this);
                build.setTitle("CHOOSE OPERATION");
                build.setMessage("Do you want to read or listen bani ?");
                build.setPositiveButton("Read", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i=new Intent(Nitnem.this,Screen3.class);
                        i.putExtra("pn",pathname);
                        startActivity(i);
                    }
                });
                build.setNegativeButton("Listen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        main.start(); }
                });build.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                build.show();

            }
        });


        ib1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);

                try {
                    if (main.isPlaying() == true) {
                        main.pause();

                    }

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "NO BANI IS BEING PLAYED", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                try {
                    if (main.isPlaying() == false) {
                        main.start();

                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "No BANI Selected", Toast.LENGTH_SHORT).show();
                }
            }
        });
      }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
                System.exit(0);


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    protected void onDestroy() {
        main.release();
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nitnem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
     switch (item.getItemId()){
         case R.id.Copyright:
             Intent ii=new Intent(Nitnem.this,CopyRight.class);
             startActivity(ii);
             break;
         case R.id.about1:
             playback();
             break;
         case R.id.update:
             Toast.makeText(getApplicationContext(),"NO UPDATE AVAILABLE",Toast.LENGTH_SHORT).show();
             break;


     }



        return true;
    }
//    private void aboutme()
    private void playback()
    {Intent abt=new Intent(Nitnem.this,About2.class);
        startActivity(abt);
     //   Toast.makeText(getApplicationContext(),"bani gall??",Toast.LENGTH_SHORT).show();
    }
}
